#include <gtk/gtk.h>

/* FIXME: global vars !? */

#define STR_SIZE 255
#define ARGV_SIZE 10
#define ARG_SIZE 15

void
create_tags 			       ();

gboolean
open_hspell_pipe                       ();

void
close_hspell_pipe                      ();

void
set_argv                           ();

void
set_def_argv                           ();

void
on_suggest_checkbutton_toggled         (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_notes_checkbutton_toggled           (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_linguistic_checkbutton_toggled      (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_order_checkbutton_toggled           (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_spell_button_clicked                (GtkButton       *button,
                                        gpointer         user_data);

void
on_close_button_clicked                (GtkButton       *button,
                                        gpointer         user_data);

void
on_info_button_clicked                 (GtkButton       *button,
                                        gpointer         user_data);

void
on_info_close_button_clicked           (GtkButton       *button,
                                        gpointer         user_data);
