#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gtk/gtk.h>

#include "callbacks.h"
#include "interface.h"
#include "support.h"

gchar *str_hspell_bin = "hspell";
gchar *str_c_flag = "-c";
gchar *str_n_flag = "-n";
gchar *str_l_flag = "-l";
gchar *str_s_flag = "-s";

gboolean c_flag;
gboolean n_flag;
gboolean l_flag;
gboolean s_flag;

gchar *hspell_argv[ARGV_SIZE];

gint hspell_standard_input;	/* file descriptor */
gint hspell_standard_output;	/* file descriptor */

GIOChannel *hspell_standard_input_channel;
GIOChannel *hspell_standard_output_channel;

/* pipeing */

void
ChildSetupFunc (gpointer user_data)
{
	/* do nothing for now */
}

void
create_tags ()
{
	gtk_text_buffer_create_tag (gtk_text_view_get_buffer
				    (GTK_TEXT_VIEW (textview)), "rtl",
				    "direction", GTK_TEXT_DIR_RTL,
				    "wrap_mode", GTK_WRAP_WORD, "indent", 3,
				    "left_margin", 2, "right_margin", 2,
				    NULL);
}

gboolean
open_hspell_pipe ()
{
	gboolean bool_hspell_call;

	/* FIXME: throw exceptions ! */

	hspell_standard_input = 0;
	hspell_standard_output = 0;
	hspell_standard_input_channel = NULL;
	hspell_standard_output_channel = NULL;

	/* open hspell */
	bool_hspell_call = g_spawn_async_with_pipes (NULL,
						     hspell_argv,
						     NULL,
						     G_SPAWN_SEARCH_PATH,
						     ChildSetupFunc,
						     NULL,
						     NULL,
						     &hspell_standard_input,
						     &hspell_standard_output,
						     NULL, NULL);

	if (!bool_hspell_call)
		return FALSE;

	/* open chanels for input/output */
	hspell_standard_input_channel =
		g_io_channel_unix_new (hspell_standard_input);
	hspell_standard_output_channel =
		g_io_channel_unix_new (hspell_standard_output);

	/* set chanels encoding to iso8859-8 */
	g_io_channel_set_encoding (hspell_standard_input_channel,
				   "iso8859-8", NULL);
	g_io_channel_set_encoding (hspell_standard_output_channel,
				   "iso8859-8", NULL);

	return TRUE;
}

void
close_hspell_pipe ()
{
	/* unreff input/output chanels */
	g_io_channel_unref (hspell_standard_input_channel);
	hspell_standard_input_channel = NULL;
	g_io_channel_unref (hspell_standard_output_channel);
	hspell_standard_output_channel = NULL;
}

void
set_argv ()
{
	int i = 1;

	if (c_flag)
	{
		hspell_argv[i] = str_c_flag;
		i++;
	}
	if (n_flag)
	{
		hspell_argv[i] = str_n_flag;
		i++;
	}
	if (l_flag)
	{
		hspell_argv[i] = str_l_flag;
		i++;
	}
	if (s_flag)
	{
		hspell_argv[i] = str_s_flag;
		i++;
	}
	hspell_argv[i] = NULL;
}

void
set_def_argv ()
{
	/* set argv  */
	c_flag = TRUE;
	n_flag = TRUE;
	l_flag = TRUE;
	s_flag = FALSE;

	hspell_argv[0] = str_hspell_bin;

	set_argv ();
}

/* end pipeing */

void
on_suggest_checkbutton_toggled (GtkToggleButton * togglebutton,
				gpointer user_data)
{
	/* set argv  */
	c_flag = !c_flag;

	set_argv ();
}


void
on_notes_checkbutton_toggled (GtkToggleButton * togglebutton,
			      gpointer user_data)
{
	/* set argv  */
	n_flag = !n_flag;

	set_argv ();
}


void
on_linguistic_checkbutton_toggled (GtkToggleButton * togglebutton,
				   gpointer user_data)
{
	/* set argv  */
	l_flag = !l_flag;

	set_argv ();
}


void
on_order_checkbutton_toggled (GtkToggleButton * togglebutton,
			      gpointer user_data)
{
	/* set argv  */
	s_flag = !s_flag;

	set_argv ();
}


void
on_spell_button_clicked (GtkButton * button, gpointer user_data)
{
	/* check spelling */
	gchar word_local[STR_SIZE];
	gchar *str_return;
	GtkTextIter start_iter;
	GtkTextIter end_iter;

	/* init */
	str_return = NULL;
	gtk_text_buffer_get_start_iter (gtk_text_view_get_buffer
					(GTK_TEXT_VIEW (textview)),
					&start_iter);
	gtk_text_buffer_get_end_iter (gtk_text_view_get_buffer
				      (GTK_TEXT_VIEW (textview)), &end_iter);

	/* get word to check */
	g_stpcpy (word_local, gtk_entry_get_text (GTK_ENTRY (word_entry)));

	/* open pipe */
	if (open_hspell_pipe ())
	{
		/* send word to hspell input */
		g_io_channel_write_chars (hspell_standard_input_channel,
					  word_local, -1, NULL, NULL);

		g_io_channel_write_chars (hspell_standard_input_channel,
					  "\n", -1, NULL, NULL);

		/* close hspell input */
		g_io_channel_shutdown (hspell_standard_input_channel, TRUE,
				       NULL);

		/* get results */
		g_io_channel_read_to_end (hspell_standard_output_channel,
					  &str_return, NULL, NULL);

		/* close hspell output */
		close_hspell_pipe ();

		/* print results in text box */

		gtk_text_buffer_delete (gtk_text_view_get_buffer
					(GTK_TEXT_VIEW (textview)),
					&start_iter, &end_iter);

		gtk_text_buffer_insert_with_tags_by_name
			(gtk_text_view_get_buffer (GTK_TEXT_VIEW (textview)),
			 &start_iter, str_return, -1, "rtl", NULL);

		/* free unused data */
		g_free (str_return);
	}
	else
	{
		/* set out an error */
		GtkWidget *dialog =
			gtk_message_dialog_new (GTK_WINDOW (hspell_window),
						GTK_DIALOG_DESTROY_WITH_PARENT,
						GTK_MESSAGE_ERROR,
						GTK_BUTTONS_CLOSE,
						_
						("Hspell-Gui can not find the hspell program on your path."));
		gtk_dialog_run (GTK_DIALOG (dialog));
		gtk_widget_destroy (dialog);

		/* print the error to std err */
		g_printerr ("hspell_gui: can't open a pipe to hspell.\n");

	}
}


void
on_close_button_clicked (GtkButton * button, gpointer user_data)
{
	gtk_main_quit ();
}


void
on_info_button_clicked (GtkButton * button, gpointer user_data)
{
	gtk_widget_show (inforamtion_window);
}


void
on_info_close_button_clicked (GtkButton * button, gpointer user_data)
{
	gtk_widget_hide (inforamtion_window);
}
