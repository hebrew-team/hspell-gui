Summary: A GTK2 front end to hspell.
Name: hspell-gui
Version: 0.2.2
Release: 1
Copyright: GPL
Group: Applications/Productivity
Source: hspell-gui-0.2.2.tar.gz

BuildRoot: /var/tmp/%{name}-buildroot
BuildArch: i386

Requires: hspell-fat
BuildRequires: hspell-fat-devel

%description
A GTK2 front end to hspell, a hebrew speller.
with GNOME 2 panel applet.

%prep

%setup

%build
./configure --prefix=%{_prefix}
make CFLAGS="$CFLAGS $RPM_OPT_FLAGS"

%install
make install DESTDIR=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
/usr/bin/hspell-gui
/usr/bin/hspell-gui-heb
/usr/libexec/hspell-applet
/usr/lib/bonobo/servers/hspell-applet.server
/usr/share/hspell-gui/pixmaps/logo.png
/usr/share/locale/he/LC_MESSAGES/hspell-gui.mo

%doc README COPYING NEWS AUTHORS ChangeLog

   
%post

%postun

%changelog
